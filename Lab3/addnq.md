create database OrderProcessing;

use OrderProcessing;

#Create Tables

create table customer(custno int, cname varchar(50), city varchar(30), primary key(custno));

create table orders(orderno int, odate date, custno int, ordamt int, primary key(orderno), foreign key(custno) references customer(custno));

create table item(itemno int, unitprice int, primary key(itemno));

create table order_items(orderno int, itemno int, qty int, primary key(orderno, itemno));

create table warehouse(warehouseno int, city varchar(30), primary key(warehouseno));

create table shipment(orderno int, warehouseno int, shipdate date, primary key(orderno, warehouseno), foreign key(warehouseno) references warehouse(warehouseno));

#Populate

insert into customer(custno, cname, city) values(223,'Jason','Bangalore');

insert into customer(custno, cname, city) values(243,'Jack','Manipal');

insert into customer(custno, cname, city) values(623,'Eathan','Chennai');

insert into customer(custno, cname, city) values(523,'Michael','Bangalore');

insert into customer(custno, cname, city) values(983,'James','Bangalore');


insert into orders(orderno, odate, custno, ordamt) values(5423,TO_DATE('2019-07-18', 'YYYY-MM-DD'), 223, 5600);

insert into orders(orderno, odate, custno, ordamt) values(4443,TO_DATE('2019-08-18', 'YYYY-MM-DD'), 983, 600);

insert into orders(orderno, odate, custno, ordamt) values(1723,TO_DATE('2019-07-08', 'YYYY-MM-DD'), 243, 500);

insert into orders(orderno, odate, custno, ordamt) values(5483,TO_DATE('2019-06-30', 'YYYY-MM-DD'), 223, 200);

insert into orders(orderno, odate, custno, ordamt) values(58883,TO_DATE('2019-08-10', 'YYYY-MM-DD'), 523, 46000);

insert into orders(orderno, odate, custno, ordamt) values(9863,TO_DATE('2019-08-22', 'YYYY-MM-DD'), 223, 3400);


insert into item(itemno, unitprice) values(333,876);

insert into item(itemno, unitprice) values(343,800);

insert into item(itemno, unitprice) values(133,46000);

insert into item(itemno, unitprice) values(673,5432);

insert into item(itemno, unitprice) values(323,7865);


insert into order_items(orderno, itemno, qty) values(5423, 343, 7);

insert into order_items(orderno, itemno, qty) values(4443, 333, 1);

insert into order_items(orderno, itemno, qty) values(1723, 343, 1);

insert into order_items(orderno, itemno, qty) values(5483, 323, 1);

insert into order_items(orderno, itemno, qty) values(58883, 133, 1);

insert into order_items(orderno, itemno, qty) values(9863, 343, 5);


insert into warehouse(warehouseno, city) values(1337,'Bangalore');

insert into warehouse(warehouseno, city) values(1777,'Hyderabad');

insert into warehouse(warehouseno, city) values(9397,'Mangalore');

insert into warehouse(warehouseno, city) values(8347,'Chennai');

insert into warehouse(warehouseno, city) values(2327,'Mumbai');


insert into shipment(orderno, warehouseno, shipdate) values(5423, 1337, TO_DATE('2019-08-28', 'YYYY-MM-DD'));

insert into shipment(orderno, warehouseno, shipdate) values(4443, 1777, TO_DATE('2019-09-02', 'YYYY-MM-DD'));

insert into shipment(orderno, warehouseno, shipdate) values(1723, 9397, TO_DATE('2019-09-09', 'YYYY-MM-DD'));

insert into shipment(orderno, warehouseno, shipdate) values(5483, 8347, TO_DATE('2019-08-27', 'YYYY-MM-DD'));

insert into shipment(orderno, warehouseno, shipdate) values(58883, 2327, TO_DATE('2019-09-30', 'YYYY-MM-DD'));

insert into shipment(orderno, warehouseno, shipdate) values(9863, 8347, TO_DATE('2019-09-03', 'YYYY-MM-DD'));

#Queries

select cus.cname,count(ord.custno) No_of_orders,avg(ord.ordamt) AVG_Order_amt from orders ord,customer cus where ord.custno=cus.custno group by ord.custno,cus.cname order by count(ord.custno);

select orderno from shipment, warehouse where city="Bangalore" and shipment.warehouseno=warehouse.warehouseno; 
