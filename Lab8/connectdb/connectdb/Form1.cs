﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace connectdb
{
    public partial class Form1 : Form
    {
        OracleConnection conn; 
        OracleCommand comm; 
        OracleDataAdapter da; 
        DataSet ds;
        DataTable dt; 
        DataRow dr;
        int i = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.CUSTOMER' table. You can move, or remove it, as needed.
            //this.cUSTOMERTableAdapter.Fill(this.dataSet1.CUSTOMER);

        }
        public void DB_Connect()
        {
            String oradb = "Data Source=ictorcl;Persist Security Info=True;User ID=cc3308;Password=student;";
            conn = new OracleConnection(oradb);
            conn.Open();
        }

        private void load_Click(object sender, EventArgs e)
        {
            DB_Connect();
            comm = new OracleCommand(); comm.CommandText = "select * from person";
            comm.CommandType = CommandType.Text;
            ds = new DataSet();
            da = new OracleDataAdapter(comm.CommandText, conn);
            da.Fill(ds, "Tbl_instructor");
            dt = ds.Tables["Tbl_instructor"];
            int t = dt.Rows.Count;
            dr = dt.Rows[i];
            did.Text = dr["DRIVER_ID"].ToString(); 
            dname.Text = dr["NAME"].ToString(); 
            daddress.Text = dr["ADDRESS"].ToString();

            conn.Close();
        }

        private void next_Click(object sender, EventArgs e)
        {
            i++;
            if (i >= dt.Rows.Count) 
                i = 0;
            load_Click(sender,e);
        }

        private void previous_Click(object sender, EventArgs e)
        {
            i--;
            if (i < 0)
                i = dt.Rows.Count-1;
            load_Click(sender, e);
        }

        private void insert_Click(object sender, EventArgs e)
        {
            DB_Connect();
            OracleCommand cm = new OracleCommand(); 
            cm.Connection = conn;
            cm.CommandText = "insert into person values('" + did.Text + "', '" + dname.Text + "','" + daddress.Text + "')"; 
            cm.CommandType = CommandType.Text;
            cm.ExecuteNonQuery(); 
            MessageBox.Show("Inserted!");
            conn.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void viewgrid_Click(object sender, EventArgs e)
        {
            DB_Connect();
            comm = new OracleCommand();
            comm.CommandText = "select * from customer";
            comm.CommandType = CommandType.Text;
            ds = new DataSet();
            da = new OracleDataAdapter(comm.CommandText, conn);
            da.Fill(ds, "Tbl_customer");
            dt = ds.Tables["Tbl_customer"];
            dr = dt.Rows[i];
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tbl_customer";
            conn.Close();
        }

        private void update_Click(object sender, EventArgs e)
        {
            DB_Connect();
            OracleCommand cm = new OracleCommand();
            cm.Connection = conn;
            cm.CommandText = "insert into person values('" + did.Text + "', '" + dname.Text + "','" + daddress.Text + "')";
            cm.CommandType = CommandType.Text;
            cm.ExecuteNonQuery();
            MessageBox.Show("Inserted!");
            conn.Close();
        }
    }
}
