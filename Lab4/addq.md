select cname from customer, orders where customer.custno=orders.custno and odate=TO_DATE('09/05/2015', 'DD/MM/YYYY');

select orderno, sum(qty) from order_items group by orderno;

with temp(val1,val2) as(select sum(qty),orderno from order_items group by orderno) select temp.val2 Orderno, max(temp.val1) maxqty from temp;
select orderno, qty from order_items where qty=(select max(qty) from order_items);

select * from (select odate, count(odate) from orders group by odate order by count(odate) desc) where rownum =1;

delete from order_items where itemno = 133;
delete from item where itemno=133;

select orderno from warehouse, shipment where warehouse.warehouseno=shipment.warehouseno and city='Bangalore';

select cname from customer where cname like '%han';

select cname,ordamt from customer,orders where orders.custno=customer.custno order by ordamt desc;

select distinct cname from customer, orders where (select count(*) from orders where customer.custno=orders.custno and odate='2019-07-08') > 3;