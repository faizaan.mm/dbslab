﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        public static String user_name1 = "";
        public static String pwd1 = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void register_Click(object sender, EventArgs e)
        {
            if (user_text.Text.Equals("") && password_text.Text.Equals(""))
                MessageBox.Show("Enter valid credentials");
            else
            {
                user_name1 = user_text.Text;
                pwd1 = password_text.Text;
            }

        }

        private void login_Click(object sender, EventArgs e)
        {
            if (user_text.Text == user_name1 && password_text.Text == pwd1)
                MessageBox.Show("Login Successful");
            else
                MessageBox.Show("enter valid info");
        }

        private void change_password_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }
    }
}
