/*Create tables*/
create table student(regno varchar(20), name varchar(50), major varchar(20), bdate date, primary key(regno));
create table course(courseno int, cname varchar(30), dept varchar(30), primary key(courseno));
create table enroll(regno varchar(20),courseno int, primary key(regno,courseno), foreign key(regno) references student(regno), foreign key(courseno) references course(courseno));
create table text(book_isbn int, booktitle varchar(50), publisher varchar(50), author varchar(50), primary key(book_isbn));
create table book_adoption(courseno int, sem int, book_isbn int, primary key(courseno,book_isbn), foreign key(courseno) references course(courseno), foreign key(book_isbn) references text(book_isbn));

/*Populate databases*/
insert into student values('123','Billy','CS',TO_DATE('1999-04-06', 'YYYY-MM-DD'));
insert into student values('333','Brandon','BIO',TO_DATE('1997-06-01', 'YYYY-MM-DD'));
insert into student values('133','Bouza','MATH',TO_DATE('2000-01-17', 'YYYY-MM-DD'));
insert into student values('185','Barney','MATH',TO_DATE('1998-06-18', 'YYYY-MM-DD'));
insert into student values('147','Blue','CS',TO_DATE('1999-05-26', 'YYYY-MM-DD'));
insert into student values('445','Red','PHY',TO_DATE('1999-09-16', 'YYYY-MM-DD'));

insert into course values('1105','OOP','CS');
insert into course values('1112','DS','CS');
insert into course values('2103','Genetics','BIO');
insert into course values('2105','Micro Bio','BIO');
insert into course values('5107','Graph Theory','MATH');
insert into course values('5112','PROBABILITY','MATH');

insert into enroll values('123','1105');
insert into enroll values('333','2103');
insert into enroll values('133','5107');
insert into enroll values('185','5112');
insert into enroll values('147','1112');
insert into enroll values('123','1112');

insert into text values(32134,'Instroduction to OOP in Java','PP Press Pvt. Ltd.','Jack Danger');
insert into text values(42324,'Data Structures and Algorithms in C++','Pearson','Mark Smith');
insert into text values(87634,'Guide to Genetics','Medilife','Alicia Hardman');
insert into text values(54134,'MicroB Applications','Oxford Press','William Gustaffssson');
insert into text values(67934,'Graphs and Applications','Oswald','Djikstraman');
insert into text values(76387,'Introduction to statistical Probability','Wallance and Co.','Willy Wonka');
insert into text values(76388,'Advanced statistical Probability','Wallance and Co.','Willy Wonka');
insert into text values(45678,'Book1','Pearson','John D');

insert into book_adoption values(1105,3,32134);
insert into book_adoption values(1112,3,42324);
insert into book_adoption values(2103,4,87634);
insert into book_adoption values(2105,3,54134);
insert into book_adoption values(5107,5,67934);
insert into book_adoption values(5112,5,76387);
insert into book_adoption values(5112,6,76388);

/*Queries*/

--Q1

select c.courseno, c.cname 
from course c
where (select count(*) from book_adoption b where b.courseno=c.courseno)>1;

select c.dept
from course c, book_adoption b, text 
where c.courseno=b.courseno and b.book_isbn in
(select t.book_isbn from text t where t.book_isbn=b.book_isbn and t.publisher="Wallance and Co.")
group by c.dept;

select s.name
from student s
where s.regno in (select regno from enroll e, course c where e.courseno = c.courseno group by regno having count(c.dept)>1); 

select name 
from student
where regno not in (select regno from enroll);

select book_isbn
from text
where book_isbn in (select book_isbn from book adoption natural join enroll group by book_isbn);

select c.dept
from course c, book_adoption b, text 
where c.courseno=b.courseno and b.book_isbn in
(select t.book_isbn from text t where t.book_isbn=b.book_isbn and t.publisher="Wallance and Co.")
group by c.dept;

with totalbooks(stud,texts) as 
(select regno,count(book_isbn)
from enroll natural join book_adoption 
group by regno)
select stud,texts from totalbooks
where texts = (select max(texts) from totalbooks);

select t.publisher, count(t.publisher)
from text t
group by t.publisher;

select name from student
where regno in (select regno from enroll natural join book_adoption group by regno);

--Q2

select c.cname
from customer c
where not exists (select d.city from customer d where c.custno=d.custno);	

select custno, cname , sum(ordamt)
from customer natural join orders
group by custno, cname 
having sum(ordamt) = (select max(ordamt) from orders);

select o.orderno 
from orders o
where not exists (select orderno from shipment);

select itemno
from item
where not exists (select itemno from order_items);

select * from (select count(custno) from orders natural join order_items group by custno order by count(custno) desc) where rownum=1;

select orderno
from orders
where not exists (select orderno from shipment where shipdate='08-JUL-2019');

select custno
from orders
where exists (select custno from shipment where shipdate='08-JUL-2019') and exists (select custno from shipment where shipdate='2019-JUL-04');

select c.*
from customer c
where not exists ((select orderno from orders where custno = c.custno) except
	 (select orderno from orders natural join shipment where warehouseno=1337 and custno=c.custno));

select * from (select count(custno) from orders group by custno order by count(custno) desc) where rownum=1;